define([
	
	'jquery',
	'lightbox',
	'lightbox-gallery'

], function( $, featherlight, featherlightGallery ){

	$(document).ready(function(){
		
		$('.lightbox').featherlightGallery({
			gallery: {
				fadeIn: 300,
				fadeOut: 300
			},
			openSpeed:    300,
			closeSpeed:   300
		});

	});

});