<?php

	use ChefSections\Wrappers\Template;

	use Cuisine\View\Image;
	use Cuisine\Utilities\Sort;

	$media = $column->getField( 'media' );
	$maxRow = $column->getField( 'posts_per_row' );
	$maxPosts = count( $media );
	$grid = $column->getField( 'grid', 'grid' );
	$link = $column->getField( 'link', 'in_lightbox' );

	if( !empty( $media ) ){

		$media = Sort::byField( $media, 'position' );

		$i = 0;
		$inRow = 0;
	
		foreach( $media as $img ){
		
			//row opener:
			if( $inRow == 0 && $grid !== 'masonry' )
				echo '<div class="gallery-row gallery-wrapper">';

					//image sizes:
					
					$full = Image::getMediaUrl( $img['img-id'], 'full' );
					$url = Image::getMediaUrl( $img['img-id'], 'thumb' );

					$class = 'gallery-item block';
					$target = '';
					$el = '<a ';
					$elc = '</a>';

					if( $link == 'in_lightbox' ){
						
						$class .= ' lightbox';
						$href = 'href="#" data-featherlight="'.$full.'"';
					
					}else if( $link == 'none' ){

						$el = '<span ';
						$elc = '</span>';
						$href = '';

					}else{

						$target = ' target="'.$link.'"';
						$href = 'href="'.$full.'"';
					}

					


					echo $el.$href.' class="'.$class.'" '.$target.'>';
						echo '<img src="'.$url.'"/>';
					echo $elc;
		

			//row close:
			$i++; $inRow++;
			if( ( $inRow == $maxRow || $i == $maxPosts ) && $grid !== 'masonry' ){
				echo '</div>';
				$inRow = 0;
			}
	
		}
		
	}