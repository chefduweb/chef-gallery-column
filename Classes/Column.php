<?php
namespace ChefGallery;

use ChefSections\Columns\DefaultColumn;
use Cuisine\Wrappers\Field;
use Cuisine\Wrappers\Script;
use Cuisine\Utilities\Url;


class Column extends DefaultColumn{

	/**
	 * The type of column
	 * 
	 * @var String
	 */
	public $type = 'gallery';


	/*=============================================================*/
	/**             Template                                       */
	/*=============================================================*/


	/**
	 * Start the gallery wrapper
	 * 
	 * @return string ( html, echoed )
	 */
	public function beforeTemplate(){

		//get the class:
		$class = 'collection gallery ';
		$class .= $this->getField( 'grid', 'grid' );
		
		echo '<div class="'.$class.'">';

	}



	/**
	 * Add javascripts to the footer, before the template
	 * and close the div wrapper
	 * 
	 * @return string ( html, echoed )
	 */
	public function afterTemplate(){

		$grid = $this->getField( 'grid', 'grid' );
		$link = $this->getField( 'link', 'in_lightbox' );


		/*
					Masonry:
		 */
		$url = Url::plugin( 'chef-sections', true ).'Assets/js/collections/';

		if( $grid == 'masonry' )
			Script::register( 'masonry_blocks', $url.'masonry', true );	


		/*
					Lightbox:
		 */
		$url = Url::plugin( 'chef-gallery-column', true ).'Assets/js/';

		if( $link == 'in_lightbox' )
			Script::register( 'trigger_lightbox', $url.'trigger', true );

					
		//closing div:
		echo '</div>';
	}


	/*=============================================================*/
	/**             Backend                                        */
	/*=============================================================*/


	/**
	 * Create the preview for this column
	 * 
	 * @return string (html,echoed)
	 */
	public function buildPreview(){

		echo '<strong>'.$this->getField( 'title' ).'</strong>';

	}


	/**
	 * Build the contents of the lightbox for this column
	 * 
	 * @return string ( html, echoed )
	 */
	public function buildLightbox(){

		$fields = $this->getFields();
		$subfields = $this->getSubFields();

		echo '<div class="main-content">';
		
			foreach( $fields as $field ){

				$field->render();

				if( method_exists( $field, 'renderTemplate' ) ){
					echo $field->renderTemplate();
				}

			}

		echo '</div>';
		echo '<div class="side-content">';
			echo '<br/><br/>';
			foreach( $subfields as $field ){

				$field->render();

			}

			$this->saveButton();

		echo '</div>';
	}


	/**
	 * Get the fields for this column
	 * 
	 * @return [type] [description]
	 */
	private function getFields(){



		$fields = array(
			'title' => Field::text( 
				'title', 
				'',
				array(
					'label' 				=> false,
					'placeholder' 			=> 'Titel',
					'defaultValue'			=> $this->getField( 'title' ),
				)
			),
			'media' => Field::media( 
				'media', //this needs a unique id 
				'Media', 
				array(
					'label'				=> 'top',
					'defaultValue' 		=> $this->getField( 'media' )
				)
			)
		);

		return $fields;

	}


	/**
	 * Get all the subfields
	 * 
	 * @return array
	 */
	private function getSubFields(){


		$grid = array(
					'grid'			=> __( 'Grid', 'chefgallery' ),
					'masonry'		=> __( 'Metselwerk', 'chefgallery' )	
		);

		$link = array(
					'in_lightbox'			=> __( 'Lightbox', 'chefgallery' ),
					'_blank'			=> __( 'Nieuwe pagina', 'chefgallery' ),
					'_self'				=> __( 'Zelfde pagina', 'chefgallery' ),
					'none'				=> __( 'Geen link', 'chefgallery' )
		);


		$fields = array(

			'posts_per_row'	=> Field::number(
				'posts_per_row',
				__( 'Aantal afbeeldingen per rij', 'chefgallery' ),
				array(
					'defaultValue'		=> $this->getField( 'posts_per_row', 4 )
				)
			),

			'grid'	=> Field::radio(
				'grid',
				__( 'Grid Type', 'chefgallery' ),
				$grid,
				array(
					'defaultValue'	=> $this->getField( 'grid', 'grid' )
				)
			),

			'link'	=> Field::radio(
				'link',
				__( 'Link Type', 'chefgallery' ),
				$link,
				array(
					'defaultValue'	=> $this->getField( 'link', 'in_lightbox' )
				)
			)


		);

		$fields = apply_filters( 'chef_sections_collection_side_fields', $fields );
		return $fields;

	}




}
