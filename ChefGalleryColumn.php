<?php
/**
 * Plugin Name: Chef Gallery Column
 * Plugin URI: http://chefduweb.nl/plugins/chef-gallery-column
 * Description: Adds a Gallery column to Chef Sections
 * Version: 1.0
 * Author: Luc Princen
 * Author URI: http://www.chefduweb.nl/
 * License: GPLv2
 * 
 * @package Cuisine
 * @category Core
 * @author Chef du Web
 */

namespace ChefGallery;

use Cuisine\Utilities\Url;
use Cuisine\Wrappers\Script;
use Cuisine\Wrappers\Sass;


class ChefGallery{

	/**
	 * Assets instance.
	 *
	 * @var \ChefGallery\ChefGallery
	 */
	private static $instance = null;


	/**
	 * Init admin events & vars
	 */
	function __construct(){

		//register column:
		$this->registerColumn();

		//load the right files
		$this->loadIncludes();

		//assets:
		$this->enqueues();


	}

	/**
	 * Register this column-type with Chef Sections
	 * 
	 * @return void
	 */
	private function registerColumn(){


		add_filter( 'chef_sections_column_types', function( $types ){

			$base = Url::path( 'plugin', 'chef-gallery-column', true );
			$types['gallery'] = array(
						'name'		=> 'Gallerij',
						'class'		=> 'ChefGallery\Column',
						'template'	=> $base.'Assets/template.php'
			);

			return $types;

		});

	}

	/**
	 * Load all includes for this plugin
	 * 
	 * @return void
	 */
	private function loadIncludes(){

		include( 'Classes/Column.php' );

	}


	/**
	 * Enqueue scripts & Styles
	 * 
	 * @return void
	 */
	private function enqueues(){

		add_action( 'init', function(){

			//scripts:
			$url = Url::plugin( 'chef-gallery-column', true ).'Assets/js/';

			Script::register( 'lightbox', $url.'lightbox', false );
			Script::register( 'lightbox-gallery', $url.'gallery', false );


			//sass:
			$url = 'chef-gallery-column/Assets/sass/';
			
			Sass::register( 'gallery', $url.'_gallery.scss', false );
			Sass::register( 'lightbox', $url.'_lightbox.scss', false );
		
		});
	}

	

	/*=============================================================*/
	/**             Getters & Setters                              */
	/*=============================================================*/



	/**
	 * Init the Assets classes
	 *
	 * @return \ChefGallery\ChefGallery
	 */
	public static function getInstance(){

	    if ( is_null( static::$instance ) ){
	        static::$instance = new static();
	    }
	    return static::$instance;
	}


}

add_action( 'chef_sections_loaded', function(){
	\ChefGallery\ChefGallery::getInstance();
});
